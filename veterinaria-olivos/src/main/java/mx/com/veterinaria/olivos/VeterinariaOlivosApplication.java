package mx.com.veterinaria.olivos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VeterinariaOlivosApplication {

	public static void main(String[] args) {
		SpringApplication.run(VeterinariaOlivosApplication.class, args);
	}

}
